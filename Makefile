version = 1.5.1
arch = arm-7

all: package

clean:
	rm -f gitea-$(version)-linux-${arch}.spk
	rm -f gitea-$(version)-linux-${arch}
	rm -f 2_create_project/INFO
	rm -rf 1_create_package/gitea/

package: gitea-$(version)-linux-${arch}.spk

gitea-$(version)-linux-${arch}.spk: gitea-$(version)-linux-${arch}
	chmod +x ./create_spk.sh
	./create_spk.sh

gitea-$(version)-linux-${arch}:
	wget https://github.com/go-gitea/gitea/releases/download/v$(version)/gitea-$(version)-linux-$(arch)
	touch $@


