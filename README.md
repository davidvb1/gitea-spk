# Note about original codebase

This is a fork from original code developed by Alexandre Espinosa Meno, https://github.com/alexandregz/gogs-spk
-  [README-orig](README-orig.md)

# Minor changes added

- Makefile added
- Remove dependency to [Git Server](https://www.synology.com/en-global/dsm/packages/Git) because internal ssh-server should be used


# Package creation

Optional: Update version in Makefile.

```plain
make 
```

# Installation

See instructions in [README-orig](README-orig.md)

Tested to work on DS218j with Gitea 1.5.1.

